//FirstView Component Constructor
function FirstView() {
	
	//Cloud Enabled
	var Cloud = require('ti.cloud');
  	Cloud.debug = true;

	//Create group of tabs
	var tabGroup = Titanium.UI.createTabGroup();
	
	//create window
	var self = Titanium.UI.createWindow({  
    	title:'Create User',
    	backgroundColor:'#fff',
    	tabBarHidden:false
	});
	
	var win2 = Titanium.UI.createWindow({  
    	title:'Find User',
    	backgroundColor:'#fff',
    	tabBarHidden:false
	});
	
	var tab1 = Titanium.UI.createTab({  
    	icon:'KS_nav_views.png',
    	title:'Create',
    	window :self
	});
	
	var tab2 = Titanium.UI.createTab({  
    	icon:'KS_nav_views.png',
    	title:'Search',
    	window : win2
	});
	
	//Vars Instantiated
	var Imported = require('requiredJS');
	var imported = new Imported();
	var outName = imported.displayName;
	var output = String.format("ahlane wa sahlane ya ") + String.format(outName);
	var dataUser = {};	//{ email: 'amine@jabeklah.com', username: 'Kippa', first_name: 'Amine', last_name: 'Benabdeljalil', password: 'ab1990', password_confirmation: 'ab1990' };
	
	// Create a Button.
	var buttonConfig = { title : 'Create User', height : 35, width : 100, top : 230 };
  	var CreateButton = Ti.UI.createButton(buttonConfig);
	self.add(CreateButton);
	
	//labels for Search Tab
	var label1 = Ti.UI.createLabel({ color:'#000000', text: "Enter your criteria:", height: 10,	width:'auto', top: 30 });
	win2.add(label1);

	
	//Create TextFields for Create Tab
		var textFieldEmail = Ti.UI.createTextField({ hintText:'Email@somemail.com',	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED, color: '#336699',	top: 30, left: 10,	width: 250, height: 30	});
		self.add(textFieldEmail);
		var textFieldFN = Ti.UI.createTextField({ hintText:'First Name',	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED, color: '#336699',	top: 70, left: 10,	width: 250, height: 30	});
		self.add(textFieldFN);
		var textFieldLN = Ti.UI.createTextField({ hintText:'Last Name',	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED, color: '#336699',	top: 110, left: 10,	width: 250, height: 30	});
		self.add(textFieldLN);
		var textFieldPass = Ti.UI.createTextField({ hintText:'Passwords',	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED, color: '#336699',	top: 150, left: 10,	width: 250, height: 30	});
		self.add(textFieldPass);
		var textFieldCPass = Ti.UI.createTextField({ hintText:'Confirm Password',	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED, color: '#336699',	top: 190, left: 10,	width: 250, height: 30	});
		self.add(textFieldCPass);
	
	//Create TextFields for Search Tab
		var searchCriterion = Ti.UI.createTextField({ hintText:'keyword in any of the fields',	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED, color: '#336699',	top: 100, width: 250, height: 30	});
		win2.add(searchCriterion);
		
	//Button for Search Tab
		var buttonConfig = { title : 'Create User', height : 35, width : 100, top : 200 };
	  	var button2 = Ti.UI.createButton(buttonConfig);
		win2.add(button2);
	
	//Button Click Listener in Create Tab	
	CreateButton.addEventListener('click', function(e) {
		
		dataUser.email = textFieldEmail.value;
		dataUser.first_name = textFieldFN.value;
		dataUser.last_name = textFieldLN.value;
		dataUser.password = textFieldPass.value;
		dataUser.password_confirmation = textFieldCPass.value;
		
		Cloud.Users.create( dataUser,
			   function (e) {
			    if (e.success) {
			        var user = e.users[0];
			        alert('Success:\\n' +
			            'id: ' + user.id + '\\n' +
			            'first name: ' + user.first_name + '\\n' +
			            'last name: ' + user.last_name);
			    } else {
			        alert('Error:\\n' +
			            ((e.error && e.message) || JSON.stringify(e)));
			    }
		});		

	});
	
	//Button Click Listener in Search tab
	button2.addEventListener('click', function(e){
		
		var criterion = searchCriterion.value;
		
		Cloud.Users.search(	{ q: criterion }, 
			function (e) {
			    if (e.success) {
			    	alert("Match found.");		        
			        for (var i = 0; i < e.users.length; i++) {
			            var user = e.users[i];
			            win2.add(
			            	Ti.UI.createLabel({
			            		color:'#000000', 
			            		text: 'id: ' + user.id + '\\n' + 'first name: ' + user.first_name + '\\n' + 'last name: ' + user.last_name, 
			            		height: 10,	
			            		width:'auto', 
			            		top: 260 + i * 30
			            	})
			            );           
			         }
			    } else {
			        alert('Error:\\n' +
			            ((e.error && e.message) || JSON.stringify(e)));
			        win2.add( Ti.UI.createLabel({ 
			        			color:'#000000', 
			        			text: (e.error && e.message) || JSON.stringify(e), 
			        			height: 10,	
			        			width:'auto', 
			        			top: 260 })
					);
			    }
		});
		
	});
	
	
	//Adding the tabs to the group
	tabGroup.addTab(tab1);
	tabGroup.addTab(tab2);
	//Open the Group of tabs
	tabGroup.open();
	//return the group of tabs for display
	return tabGroup;
}

module.exports = FirstView;
